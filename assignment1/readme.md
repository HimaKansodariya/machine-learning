# Model Training Assignment

In this exercise we need to train a model for given linear equation y=5x+3

### Understanding 

Here, dataset is not given. 

* for x, take a list of random numbers.
* for y, calculate values of y by putting value of x in given equation

now, we have DataFrame of x and y. split it into training data and testing data. usa training data to train the model. use testing data to predict the value.


## models used:

* ### Linear regression model

    import the LinearRegressionModel from sklearn and fit training data
    
    * Co-efficient: 5.
    * intercept: 2.9999999999943
    * training score: 1
    * testing score: 1
    * time for training = 0.002001 seconds
    * graph: straight line 
    
    ##### test cases
    
    ###### 1) Adding noise
    
    * Minor change in co-efficient 
    * noticable chnage in intercept
    * training score: decrease (minor) 
    * testing score: decrease (minor) 
    * time for training: Almost same
    * graph: straight line 
    
    ###### 2) Outlier
    
    * Minor change in co-efficient
    * major change in inercept 
    * training score: decrease 
    * testing score: decrease
    * time for training: Almost same
    * graph: straight line 
    
* ### DecisionTreeRegressor model

    import the DecisionTreeRegressor from sklearn and fit training data
    
    * training score: 1
    * testing score: 1
    * time for training = 0.00499 seconds
    * graph: Thick straight line 
           
    ##### test cases
    
    ###### 1) Adding noise
    
    * training score: 1
    * testing score: decrese (minor)
    * time for training = Almost same
    * graph: scetered lines with one thick straight line
    
    ###### 2) Outlier
    
    * training score: decrese (minor)
    * testing score: decrese (minor)
    * time for training = Almost same
    * graph: scetered lines with one thick straight line
    
* ### RandomForestRegressor model

    import the RandomForestRegressor from sklearn and fit training data
    
    * training score: 0.9999
    * testing score: 1
    * time for training = 0.01299 seconds
    * graph: Thick straight line 
           
    ##### test cases
    
    ###### 1) Adding noise
    
    * training score: 0.994
    * testing score: 1
    * time for training = decrese (minor)
    * graph: multiple lines with one thick straight line
    
    ###### 2) Outlier
    
    * training score: 1
    * testing score: decrese (minor)
    * time for training = decrese 
    * graph: scetered lines with one thick straight line
    
* ### SupportVectorRegressor model

    import the SVR from sklearn and fit training data
    
   * Co-efficient: 4.9999
    * intercept: 3.00047
    * training score: 0.999
    * testing score: 1.0
    * time for training = 0.0020010 seconds
    * graph: thin straight line 
    
    ##### test cases
    
    ###### 1) Adding noise
    
    * Co-efficient: almost same
    * intercept: almost same
    * training score: decrese (minor)
    * testing score: decrese (minor)
    * time for training = increse (minor)
    * graph: thin straight line 
    
    ###### 2) Outlier
    
    * Co-efficient: Almost same
    * intercept: Almost
    * training score: decrese (minor)
    * testing score: decrese
    * time for training = increse (minor)
    * graph: Thin straight line 
    
    
    
* #### After noticing behavirour of all models in different test cases

    * Decision tree does not work well with noise and outlier in data
    * SVR does not get affected by outliers
    * linear regression uses minimun time to compute right prediction but change in intercept is big
    * SVR does not chnage co eiffiect and intercept majorly 
    
    
    
    
    
   